from setuptools import setup

setup(name='python_sample_project',
      version='0.1',
      description='sample for study',
      url='https://bitbucket.org/eveleech/python_sample_project',
      author='Solomka',
      author_email='artyomka@solom.ka',
      license='MIT',
      packages=['python_sample_project'],
      install_requires=[], # dependancy packages m.b. GitHub
      zip_safe=False)

# Details
# https://packaging.python.org/tutorials/distributing-packages/